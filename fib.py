n = int(input('Enter a number of Fibonacci digits to solve with loops '))


def fibL(n):
    x = 0
    y = 1

    for i in range(0, n):
        z = x
        x = y
        y = z + y
    return x


if n <= 0:
    print("Enter a positive integer, dummy")
else:
    for c in range(0, n):
        print(fibL(c))

n = int(input('Enter a number of Fibonacci digits to solve with recursion '))


def fibR(n):
    if n <= 1:
        return n
    else:
        return fibR(n - 1) + fibR(n - 2)


if n <= 0:
    print("Enter a positive integer, dummy")
else:
    for i in range(n):
        print(fibR(i))
